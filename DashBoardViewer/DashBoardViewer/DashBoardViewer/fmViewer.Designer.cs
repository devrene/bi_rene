﻿namespace DashBoardViewer
{
    partial class fmViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.dsvPerfilNegociador = new DevExpress.DashboardWin.DashboardViewer();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.dsvMetaMilContratos = new DevExpress.DashboardWin.DashboardViewer();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsvPerfilNegociador)).BeginInit();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaMilContratos)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 0);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1013, 469);
            this.tabPane1.SelectedPage = this.tabNavigationPage1;
            this.tabPane1.Size = new System.Drawing.Size(1013, 469);
            this.tabPane1.TabIndex = 4;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Perfil Negociador";
            this.tabNavigationPage1.Controls.Add(this.dsvPerfilNegociador);
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Size = new System.Drawing.Size(995, 424);
            // 
            // dsvPerfilNegociador
            // 
            this.dsvPerfilNegociador.DashboardSource = new System.Uri("\\\\10.1.1.10\\Casa Rene mysql\\mydashboards\\Atendentes\\Parameter\\PerfilNegociador.xm" +
        "l", System.UriKind.Absolute);
            this.dsvPerfilNegociador.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dsvPerfilNegociador.Location = new System.Drawing.Point(0, 0);
            this.dsvPerfilNegociador.Name = "dsvPerfilNegociador";
            this.dsvPerfilNegociador.Size = new System.Drawing.Size(995, 424);
            this.dsvPerfilNegociador.TabIndex = 4;
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Meta 1000 Contratos";
            this.tabNavigationPage2.Controls.Add(this.dsvMetaMilContratos);
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Size = new System.Drawing.Size(995, 424);
            // 
            // dsvMetaMilContratos
            // 
            this.dsvMetaMilContratos.DashboardSource = new System.Uri("\\\\10.1.1.10\\Casa Rene mysql\\mydashboards\\Meta\\Meta1000Contratos.xml", System.UriKind.Absolute);
            this.dsvMetaMilContratos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dsvMetaMilContratos.Location = new System.Drawing.Point(0, 0);
            this.dsvMetaMilContratos.Name = "dsvMetaMilContratos";
            this.dsvMetaMilContratos.Size = new System.Drawing.Size(995, 424);
            this.dsvMetaMilContratos.TabIndex = 0;
            // 
            // fmViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 469);
            this.Controls.Add(this.tabPane1);
            this.Name = "fmViewer";
            this.Text = "Indicadores";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsvPerfilNegociador)).EndInit();
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaMilContratos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.DashboardWin.DashboardViewer dsvPerfilNegociador;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.DashboardWin.DashboardViewer dsvMetaMilContratos;
    }
}

