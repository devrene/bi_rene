﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

namespace DashBoardViewer
{
    public partial class frmLogin : Form
    {
        SqlConnection sqlConn;
        string curDir;
        string curPath;
        string DiretorioAtualizacao = @"\\10.1.1.10\Viewer";
        private string _sql = string.Empty;
        public bool logado = true;
        public bool atualizar = false;
        public int atendenteId;

        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(@"\\10.1.1.10\Viewer");

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogar_Click(object sender, EventArgs e)
        {
            logar();

            if (logado)
             {
                Atualizacao();
                

            }
        }

        public void Atualizacao()
        {
            
            
            
            bool res = ExistFileAtualizacao();

            if (res)
            {
                string VersaoAtualizacao = GetVersionAtualizacao();
                string VersaoLocal = GetVersionLocal();
            
                if (VersaoAtualizacao != "" && VersaoAtualizacao != VersaoLocal)
                {
                    if (MessageBox.Show("Deseja Atualiza?", "Atualização", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        progressbar.Visible = true;
                        Atualizar(VersaoAtualizacao);

                    }
                    else
                        this.Dispose();
                }
                else
                    this.Dispose();
            }
            else
            {
                // MessageBox.Show("Não foi Possível se Conectar para verificar Atualização. ");
                this.Dispose();
            }      

        }

        private bool ExistFileAtualizacao()
        {
            
            bool ExistsFile = false;
            //NetworkCredential theNetworkCredential = new NetworkCredential("administrator", "@info2019@#$", "10.1.1.10");
            NetworkCredential theNetworkCredential = new NetworkCredential(@"casa\bi", "b123#");
            CredentialCache theNetCache = new CredentialCache();
            theNetCache.Add(new Uri(@"\\10.1.1.10"), "Basic", theNetworkCredential);

            foreach (System.IO.FileInfo f in di.GetFiles())
            {
                if (f.Extension.ToLower() == ".exe")
                {
                    ExistsFile = true;
                }
            }
            return ExistsFile;
        }

        public void logar()
        {
            logado = false;

            bool res = ConectarServidor();

            if (res)
            {

                string usu, pwd;
                try
                {
                    usu = tbUsuario.Text;
                    pwd = tbSenha.Text;

                    string _sql =
                        String.Format("select atendenteId, tipo_usuario from bi_usuario where usuario = '{0}' and senha = '{1}'",
                        usu, pwd);

                    SqlCommand cmd = new SqlCommand(_sql, sqlConn);

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        atendenteId = Convert.ToInt32(reader["atendenteId"]);
                    }

                    
                    //atendenteId = Convert.ToInt32(cmd.ExecuteScalar());

                    if (atendenteId > 0)
                    {
                        logado = true;
                        MessageBox.Show("Logado com Sucesso");

                    }
                    else
                    {
                        MessageBox.Show("Sua conta ou senha está incorreta. ");
                        sqlConn.Close();

                    }

                }
                catch (SqlException erro)
                {
                    MessageBox.Show(erro + " No Banco");
                    this.Dispose();
                }
            }
            else
            {
                this.Dispose();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private string GetVersionAtualizacao()
        {           
            string VersaoAtualizacao = "";
            foreach (System.IO.FileInfo f in di.GetFiles())
            {
                if (f.Extension.ToLower() == ".exe")
                {
                    VersaoAtualizacao = f.Name;
                }
            }
            return VersaoAtualizacao;            
        }

        private string GetVersionLocal()
        {
            return "Atualiza Viewer " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + ".exe";
        }

        private bool ConectarServidor()
        {
            try
            {
                
                sqlConn =
                   new SqlConnection("Data Source = 10.1.1.10; Integrated Security = False; User ID = suporte; Password = 123; Connect Timeout = 15; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
                
                sqlConn.Open();

                return true;

            }
            catch (Exception)
            {
                MessageBox.Show("Não foi Possivel se Conectar ao Servidor!");
                return false;
            }
        }

        private void Atualizar(string versao)
        {
            string _directoryPath = DiretorioAtualizacao + "\\" + versao;                        
            curPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory.ToString());
            DeletaArquivoAtualizacao();

            curDir = curPath + "\\" + versao;            

            FileInfo fi = new FileInfo(_directoryPath);
            fi.CopyTo(curDir, true);

            Process.Start(curDir);
            atualizar = true;
            this.Dispose();


        }

        private void DeletaArquivoAtualizacao()
        {
            System.IO.DirectoryInfo diretorio = new System.IO.DirectoryInfo(@curPath);

            foreach (System.IO.FileInfo file in diretorio.GetFiles("Atualiza" + "*"))
            {                
               File.Delete(file.Name);               
            }
        }
    }
}
