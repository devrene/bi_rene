﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DashBoardViewer
{
    public partial class fmViewer : Form
    {
        public fmViewer(int atendenteId)
        {
            InitializeComponent();
            dsvPerfilNegociador.Parameters["atendente"].Value = atendenteId;
            dsvMetaMilContratos.Parameters["atendente"].Value = atendenteId;
            dsvMetaMilContratos.Parameters["diretoria"].Value = 'n';
            
        }
    }
}
