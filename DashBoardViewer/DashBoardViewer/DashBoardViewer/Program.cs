﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DashBoardViewer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            


            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("pt-BR");


            frmLogin frmlogin = new frmLogin();
            frmlogin.ShowDialog();

            if (frmlogin.logado && !(frmlogin.atualizar))
            {
                Application.Run(new fmViewer(frmlogin.atendenteId));
              //  BonusSkins.Register();
              //  SkinManager.EnableFormSkins();
                
            }
        }
    }
}
