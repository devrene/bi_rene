      <Connection Name="localhost_casaimoveis_Connection" ProviderKey="MySql">
        <Parameters>
          <Parameter Name="server" Value="localhost" />
          <Parameter Name="database" Value="casaimoveis" />
          <Parameter Name="read only" Value="1" />
          <Parameter Name="generateConnectionHelper" Value="false" />
          <Parameter Name="Port" Value="3306" />
          <Parameter Name="userid" Value="root" />
          <Parameter Name="password" Value="root" />
        </Parameters>
      </Connection>