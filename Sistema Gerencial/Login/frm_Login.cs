﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace dxaDashBoardsMain
{
    public partial class frmLogin : Form
    {
        MySqlConnection sqlConn = null;
        private string strConn = 
            String.Format("server={0};user id={1}; password={2}; database=casaimoveis; pooling=false","localhost", "root", "root");

        private string _sql = string.Empty;
        public bool logado = false;

        public frmLogin()
        {
            InitializeComponent();
        }
        public void logar()
        {
            sqlConn = new MySqlConnection(strConn);
            sqlConn.Open();
            string usu, pwd;
            try
            {
                usu = tbUsuario.Text;
                pwd = tbSenha.Text;

                string _sql = 
                    String.Format("select count(id_usuario) from _bi_usuario where usuario = '{0}' and senha = '{1}'",
                    usu, pwd);

                MySqlCommand cmd = new MySqlCommand(_sql, sqlConn);
                Int32 count = Convert.ToInt32(cmd.ExecuteScalar());

                if (count > 0)
                {
                    logado = true;
                    MessageBox.Show("Logado com Sucesso");
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Erro ao logar");
                    logado = false;
                }

            }
            catch(SqlException erro)
            {
                MessageBox.Show(erro + " No Banco");
            }
        }

        private void btnLogar_Click(object sender, EventArgs e)
        {
            logar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
