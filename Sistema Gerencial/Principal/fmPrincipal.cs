﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Navigation;
using System.Data.SqlClient;
using System.Xml;
using DevExpress.DashboardWin;

namespace SisGerencial
{
    public partial class frmConsultaAdm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        string DataSourceFocused;
        private string info;
        

        public frmConsultaAdm()
        {
            
            InitializeComponent();
            foreach (var element in accordionControl.GetElements())
            {              
                element.Visible = true;
            }
        }
        void FocusDashBoardView(AccordionControlElement e, bool refresh)
        {
            XtraUserControl userControl;

            #region "load xml database"

            /*  SqlConnection sqlConn =
                new SqlConnection("Server=192.168.15.4,1431; Database=dw_casaimoveis; User ID=sa; Password=123; Trusted_Connection=False");
              sqlConn.Open();
              SqlCommand custCMD = new SqlCommand("SELECT dashboard FROM [bi_dashboard] ", sqlConn);

              string s = "";

              //System.Xml.XmlReader myXR = custCMD.ExecuteXmlReader();
              using (XmlReader reader = custCMD.ExecuteXmlReader())
              {
                  while (reader.Read())
                  {
                      s = reader.ReadOuterXml();
                      // do something with s
                  }
              }*/

            #endregion

            
            //DataSourceFocused = "////localhost\\local_dashboard\\";
            DataSourceFocused = "////10.1.1.10\\Casa Rene mysql\\mydashboards\\";



            DashboardViewer dsvContratosResisoes = new DashboardViewer();

            



            #region verifica item selecionado
            switch (e.Name)
              {
                  //Taxas
                  case "aceTaxaAdministracao":
                      {
                          info = "Cenário que demonstra faturamento real gerado pela taxa de adminstração.";
                          DataSourceFocused = DataSourceFocused + "Taxas\\Taxa T.A Acumulada.xml";
                          if (dsvTaxaAdministracao.DashboardSource == null || refresh)
                              dsvTaxaAdministracao.DashboardSource =
                                  new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                          userControl = dsvTaxaAdministracao;

                          break;
                      }

                  case "aceTaxaIntermediacao":
                      {
                        info = "Cenário que demonstra faturamento real gerado pela taxa de Intermediação.";
                        DataSourceFocused = DataSourceFocused + "Taxas\\Taxa T.I Mensal.xml";
                          if (dsvTaxaIntermediacao.DashboardSource == null || refresh)
                              dsvTaxaIntermediacao.DashboardSource =
                                  new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                          userControl = dsvTaxaIntermediacao;

                          break;
                      }

                case "aceJurosMulta":
                    {
                        info = "Cenário que demonstra os valores arrecadados atrávez de Juros e Multa.";
                        DataSourceFocused = DataSourceFocused + "Taxas\\Taxa Juros Multa Mensal.xml";
                        if (dsvJurosMulta.DashboardSource == null || refresh)
                            dsvJurosMulta.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvJurosMulta;

                        break;
                    }
                //Atendente
                case "aceAtendenteCardTA":
                    {
                        info = "Cenário que demonstra faturamento real gerado pela taxa de Adminstração por colaborador.";
                        DataSourceFocused = DataSourceFocused + "Atendentes\\atendente_Cards_TA.xml";
                        if (dsvAtendenteCardTA.DashboardSource == null || refresh)
                            dsvAtendenteCardTA.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvAtendenteCardTA;

                        break;
                    }
                case "aceAtendenteCardTI":
                    {
                        info = "Cenário que demonstra faturamento real gerado pela taxa de Intermediação por colaborador.";
                        DataSourceFocused = DataSourceFocused + "Atendentes\\atendente_Cards_TI.xml";
                        if (dsvAtendenteCardTI.DashboardSource == null || refresh)
                            dsvAtendenteCardTI.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvAtendenteCardTI;

                        break;
                    }
                case "aceAtendenteGrafico":
                    {
                        info = "Cenário que demonstra uma previsão de produção da equipe ou individual por colaborador.";
                        DataSourceFocused = DataSourceFocused + "Atendentes\\atendente_Grafico.xml";
                        if (dsvAtendenteGrafico.DashboardSource == null || refresh)
                            dsvAtendenteGrafico.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvAtendenteGrafico;

                        break;
                    }
                //Contratos
                case "aceBairroGrafico":
                    {
                        info = "Cenário que demonstra a quantidade de imóvel ativos por bairros, classificados por tipo, ticket médio, percentual e valores.";
                        DataSourceFocused = DataSourceFocused + "Contratos\\Contratos Ativos Bairros.xml";
                        if (dsvBairroGrafico.DashboardSource == null || refresh)
                            dsvBairroGrafico.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvBairroGrafico;

                        break;
                    }
                case "aceResisoes":
                    {
                        info = "Cenário que demonstra a quantidade de contratos que foram rescindidos e o valor de T.A que foi perdido devido essas rescisões.";
                        DataSourceFocused = DataSourceFocused + "Contratos\\Resisoes.xml";
                        if (dsvContratosResisoes.DashboardSource == null || refresh)
                            dsvContratosResisoes.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvContratosResisoes;

                        break;
                    }
                case "aceContratosPendentes":
                    {
                        info = "Cenário que demonstra os titulos a receber.";
                        DataSourceFocused = DataSourceFocused + "Contratos\\Contratos Atrasados.xml";
                        if (dsvContratosPendentes.DashboardSource == null || refresh)
                            dsvContratosPendentes.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvContratosPendentes;

                        break;
                    }
                case "aceContratosAcumulados":
                    {
                        info = "Cenário que demonstra a quantidade de contratos e percentual de crescimento.";
                        DataSourceFocused = DataSourceFocused + "Contratos\\Contratos Acumulados.xml";
                        if (dsvContratosAcumulados.DashboardSource == null || refresh)
                            dsvContratosAcumulados.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvContratosAcumulados;

                        break;                       
                    }
                case "aceContratoPag":
                    {
                        info = "Cenário que demonstra a quantidade de contratos e percentual de contratos recebidos a vencer e vencidos.";
                        DataSourceFocused = DataSourceFocused + "Contratos\\Contratos Pagamento.xml";
                        if (dsvContratopag.DashboardSource == null || refresh)
                            dsvContratopag.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvContratopag;

                        break;
                    }
                //Financeiro 
                case "aceLucroLiquido":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Lucro Liquido.xml";
                        if (dsvFinanceiroLucroLiquido.DashboardSource == null || refresh)
                            dsvFinanceiroLucroLiquido.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvFinanceiroLucroLiquido;

                        break;
                    }
                case "acePagRecDetalhado":
                    {
                        info = "Cenário que demonstra os valores gerados para contas a pagar e contas a receber, permitindo detalhamento.";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Pag x Rec (Detalhado).xml";
                        if (dsvFinanPagRecD.DashboardSource == null || refresh)
                              dsvFinanPagRecD.DashboardSource = 
                                  new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                          userControl = dsvFinanPagRecD;

                        break;
                    }
                case "acePagRecDetalhadoFilial":
                    {
                        info = "Cenário que demonstra os valores gerados para contas a pagar e contas a receber, permitindo detalhamento.(Filial)";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Pag x Rec (Detalhado) Filial.xml";
                        if (dsvFinanPagRecDetalheFilial.DashboardSource == null || refresh)
                            dsvFinanPagRecDetalheFilial.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvFinanPagRecDetalheFilial;

                        break;
                    }
                case "acePagRecPeriodo":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Pag x Rec (Periodo).xml";
                        if (dsvFinanceiroPagRecP.DashboardSource == null || refresh)
                            dsvFinanceiroPagRecP.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvFinanceiroPagRecP;

                        break;
                    }

                case "aceCustoAtendente":
                    {
                        info = "Cenário que demonstra o custo/lucro da equipe ou individualmente os colaboradores.";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Atendente Custo.xml";
                        if (dsvCustoAtendente.DashboardSource == null || refresh)
                            dsvCustoAtendente.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvCustoAtendente;

                        break;
                    }

                case "aceVistoriaFinal":
                    {
                        info = "Cenário que demonstra o prestador e os serviços realizados na vistoria final.";
                        DataSourceFocused = DataSourceFocused + "Contratos\\Contrato Vistoria Final.xml";
                        if (dsvVistoria.DashboardSource == null || refresh)
                            dsvVistoria.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvVistoria;

                        break;
                    }

                case "aceCapitalizacao":
                    {
                        info =  "Cenário que demonstra valores arrecados com captalização imobiliária.";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Capitalização.xml";
                        if (dsvCapitalizacao.DashboardSource == null || refresh)
                            dsvCapitalizacao.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvCapitalizacao;

                        break;
                    }

                case "aceCheques":
                    {

                        info = "";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Repasse Cheques.xml";
                        if (dsvCheques.DashboardSource == null || refresh)
                            dsvCheques.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvCheques;

                        break;
                    }

                case "aceSaldoDia":
                    {

                        info = "";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Saldo Diario.xml";
                        if (dsvSaldoDia.DashboardSource == null || refresh)
                            dsvSaldoDia.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvSaldoDia;

                        break;
                    }

                case "aceMetaTI2018_01":
                    {
                        info = "Cenário que demonstra o desempenho dos atendentes e uma visão geral da meta estipulada para 1º Semestre de 2018";
                        DataSourceFocused = DataSourceFocused + "Meta\\Meta TI 2018_01.xml";
                        if (dsvMetaTI2018_1.DashboardSource == null || refresh)
                            dsvMetaTI2018_1.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvMetaTI2018_1;

                        break;
                    }

                case "aceMetaTI2018_02":
                    {
                        info = "Cenário que demonstra o desempenho dos atendentes e uma visão geral da meta estipulada para 2º Semestre de 2018";
                        DataSourceFocused = DataSourceFocused + "Meta\\Meta TI 2018_02.xml";
                        if (dsvMetaTI2018_2.DashboardSource == null || refresh)
                            dsvMetaTI2018_2.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvMetaTI2018_2;

                        break;
                    }

                case "aceMeta1000Contratos":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Meta\\Meta1000Contratos.xml";
                        if (dsvMeta1000.DashboardSource == null || refresh)
                            dsvMeta1000.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvMeta1000;

                        break;
                    }
                case "aceRobsonVisaoGeral":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Meta\\2020\\MetasRobsonVisãoGeral.xml";
                        if (dsvRobsonVisaoGeral.DashboardSource == null || refresh)
                            dsvRobsonVisaoGeral.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvRobsonVisaoGeral;

                        break;
                    }
                case "aceMetaQuantidadeContratos":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Meta\\2020\\MetaQuantidadeContratos.xml";
                        if (dsvMetaQuantidadeContratos.DashboardSource == null || refresh)
                            dsvMetaQuantidadeContratos.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvMetaQuantidadeContratos;

                        break;
                    }
                case "acePremiacaoAtendente":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Meta\\2020\\PremiacaoAtendetes.xml";
                        if (dsvPremiacaoAtendente.DashboardSource == null || refresh)
                            dsvPremiacaoAtendente.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvPremiacaoAtendente;

                        break;
                    }

                case "aceMetaLideres50":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Meta\\2020\\MetaLideres50%.xml";
                        if (dsvMetaLideres50.DashboardSource == null || refresh)
                            dsvMetaLideres50.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvMetaLideres50;

                        break;
                    }

                case "aceMetaLideres100":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Meta\\2020\\MetaLideres100%.xml";
                        if (dsvMetaLideres100.DashboardSource == null || refresh)
                            dsvMetaLideres100.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvMetaLideres100;

                        break;
                    }

                case "aceDRE":
                    {
                        info = "";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\DRE.xml";
                        if (dsv_dre.DashboardSource == null || refresh)
                            dsv_dre.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsv_dre;

                        break;
                    }

                case "aceProjecao":
                    {
                        info = "Cenário que demonstra o desempenho dos atendentes e uma visão geral da meta estipulada para 2º Semestre de 2018";
                        DataSourceFocused = DataSourceFocused + "Financeiro\\Projecao.xml";
                        if (dsv_projecao.DashboardSource == null || refresh)
                            dsv_projecao.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsv_projecao;

                        break;
                    }

                

                case "aceAtendenteProjecao":
                    {
                        //info = "Cenário que demonstra o desempenho dos atendentes e uma visão geral da meta estipulada para 2º Semestre de 2018";
                        DataSourceFocused = DataSourceFocused + "Atendentes\\ProjecaoNegociador.xml";
                        if (dsv_atendente_proj.DashboardSource == null || refresh)
                            dsv_atendente_proj.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsv_atendente_proj;

                        break;
                    }

                case "aceAtendentePerfil":
                    {
                        //info = "Cenário que demonstra o desempenho dos atendentes e uma visão geral da meta estipulada para 2º Semestre de 2018";
                        DataSourceFocused = DataSourceFocused + "Atendentes\\PerfilNegociador.xml";
                        if (dsv_atendente_perfil.DashboardSource == null || refresh)
                            dsv_atendente_perfil.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsv_atendente_perfil;

                        break;
                    }


                case "aceProducaoAgenciador":
                    {
                        //info = "Cenário que demonstra o desempenho dos atendentes e uma visão geral da meta estipulada para 2º Semestre de 2018";
                        DataSourceFocused = DataSourceFocused + "Atendentes\\ProducaoAgenciador.xml";
                        if (dsvProducaoAgenciador.DashboardSource == null || refresh)
                            dsvProducaoAgenciador.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvProducaoAgenciador;

                        break;
                    }

                case "aceVisaoGeralComissoes":
                    {
                       
                        DataSourceFocused = DataSourceFocused + "Atendentes\\Comissoes\\VisaoGeralComissoes.xml";
                        if (dsvVisaoGeralComissoes.DashboardSource == null || refresh)
                            dsvVisaoGeralComissoes.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvVisaoGeralComissoes;

                        break;
                    }

                case "aceDetalhamentoComissoes":
                    {
                        
                        DataSourceFocused = DataSourceFocused + "Atendentes\\Comissoes\\DetalhamentoComissoes.xml";
                        if (dsvDetalhamentoComissoes.DashboardSource == null || refresh)
                            dsvDetalhamentoComissoes.DashboardSource =
                                new System.Uri(DataSourceFocused, System.UriKind.Absolute);

                        userControl = dsvDetalhamentoComissoes;

                        break;
                    }





                default:
                      userControl = null;
                      break;
              }
            #endregion

            tabbedView.AddDocument(userControl);
            tabbedView.ActivateDocument(userControl);
        }
        private void accordionControl_SelectedElementChanged(object sender, DevExpress.XtraBars.Navigation.SelectedElementChangedEventArgs e)
        {
            if (e.Element == null) return;

            FocusDashBoardView(e.Element, false);
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            AccordionControlElement element;

            element = accordionControl.SelectedElement;

            if (element != null)
              FocusDashBoardView(element, true);
        }

        private void dsvVistoria_DashboardItemControlCreated(object sender, DevExpress.DashboardWin.DashboardItemControlEventArgs e)
        {
            if (e.PivotGridControl != null)
                e.PivotGridControl.BestFit();
        }

        private void dsvVistoria_DashboardItemControlUpdated(object sender, DevExpress.DashboardWin.DashboardItemControlEventArgs e)
        {
            if (e.PivotGridControl != null)
                e.PivotGridControl.BestFit();
        }


        private void btnInfo_Click(object sender, EventArgs e)
        {
            if (info != null)
                System.Windows.Forms.MessageBox.Show(info);
        }

    }
}
