﻿namespace SisGerencial
{
    partial class frmConsultaAdm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultaAdm));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.accordionControl = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.aceGroupHistorico = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceTaxaAdministracao = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceTaxaIntermediacao = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceJurosMulta = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceResisoes = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceAtendentes = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceAtendenteCardTI = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceAtendenteCardTA = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceAtendentePerfil = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceAtendenteProjecao = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceProducaoAgenciador = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceComercialP = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceAtendenteGrafico = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceContratos = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceBairroGrafico = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceContratosAcumulados = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceMetas = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceMetaTI2018_01 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceMetaTI2018_02 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceMeta1000Contratos = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceFinanceiro = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceSaldoDia = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceDRE = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceContratosPendentes = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceContratoPag = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceCapitalizacao = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceProjecao = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceVistoriaFinal = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.documentManager = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.dsvContratosResisoess = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvTaxaAdministracao = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvTaxaIntermediacao = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvAtendenteCardTA = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvAtendenteGrafico = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvBairroGrafico = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.dsvContratosAtivos = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvFinanceiroLucroLiquido = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvFinanPagRecD = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvFinanceiroPagRecP = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.dsvContratosPendentes = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvCustoAtendente = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvVistoria = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvCapitalizacao = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvContratosAcumulados = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvFinanPagRecDetalheFilial = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvCheques = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvAtendenteCardTI = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvJurosMulta = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.btnInfo = new System.Windows.Forms.Button();
            this.dsvMetaTI2018_1 = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvMetaTI2018_2 = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvContratopag = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvSaldoDia = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsv_dre = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsv_projecao = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.accordionControlElement6 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.dsv_atendente_proj = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsv_atendente_perfil = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvMeta1000 = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvProducaoAgenciador = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement2 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement3 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement4 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceRobsonVisaoGeral = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceMetaQuantidadeContratos = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.acePremiacaoAtendente = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.dsvRobsonVisaoGeral = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvMetaQuantidadeContratos = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvPremiacaoAtendente = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.aceMetaLideres50 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.dsvMetaLideres50 = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.aceMetaLideres100 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.dsvMetaLideres100 = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.accordionControlElement5 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceVisaoGeralComissoes = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.aceDetalhamentoComissoes = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.dsvVisaoGeralComissoes = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.dsvDetalhamentoComissoes = new DevExpress.DashboardWin.DashboardViewer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanel.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosResisoess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvTaxaAdministracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvTaxaIntermediacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvAtendenteCardTA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvAtendenteGrafico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvBairroGrafico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosAtivos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanceiroLucroLiquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanPagRecD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanceiroPagRecP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosPendentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvCustoAtendente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvVistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvCapitalizacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosAcumulados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanPagRecDetalheFilial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvCheques)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvAtendenteCardTI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvJurosMulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaTI2018_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaTI2018_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratopag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvSaldoDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_dre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_projecao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_atendente_proj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_atendente_perfil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMeta1000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvProducaoAgenciador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvRobsonVisaoGeral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaQuantidadeContratos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvPremiacaoAtendente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaLideres50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaLideres100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvVisaoGeralComissoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvDetalhamentoComissoes)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(885, 143);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanel
            // 
            this.dockPanel.Controls.Add(this.dockPanel1_Container);
            this.dockPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel.ID = new System.Guid("df633bc4-1c6c-46c5-b9fa-55e4354fcbf0");
            this.dockPanel.Location = new System.Drawing.Point(0, 143);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Options.ShowCloseButton = false;
            this.dockPanel.OriginalSize = new System.Drawing.Size(227, 200);
            this.dockPanel.Size = new System.Drawing.Size(227, 351);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.accordionControl);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(218, 324);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // accordionControl
            // 
            this.accordionControl.AllowItemSelection = true;
            this.accordionControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.accordionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accordionControl.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceGroupHistorico,
            this.aceAtendentes,
            this.aceComercialP,
            this.aceContratos,
            this.aceMetas,
            this.aceFinanceiro});
            this.accordionControl.Location = new System.Drawing.Point(0, 0);
            this.accordionControl.Name = "accordionControl";
            this.accordionControl.Size = new System.Drawing.Size(218, 324);
            this.accordionControl.TabIndex = 2;
            this.accordionControl.Text = "accordionControl";
            this.accordionControl.SelectedElementChanged += new DevExpress.XtraBars.Navigation.SelectedElementChangedEventHandler(this.accordionControl_SelectedElementChanged);
            // 
            // aceGroupHistorico
            // 
            this.aceGroupHistorico.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceTaxaAdministracao,
            this.aceTaxaIntermediacao,
            this.aceJurosMulta,
            this.aceResisoes});
            this.aceGroupHistorico.Name = "aceGroupHistorico";
            this.aceGroupHistorico.Text = "Comercial Realizado";
            this.aceGroupHistorico.Visible = false;
            // 
            // aceTaxaAdministracao
            // 
            this.aceTaxaAdministracao.Name = "aceTaxaAdministracao";
            this.aceTaxaAdministracao.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceTaxaAdministracao.Text = "Taxa Administração Real";
            // 
            // aceTaxaIntermediacao
            // 
            this.aceTaxaIntermediacao.Name = "aceTaxaIntermediacao";
            this.aceTaxaIntermediacao.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceTaxaIntermediacao.Text = "Taxa Intermediacao Real";
            // 
            // aceJurosMulta
            // 
            this.aceJurosMulta.Name = "aceJurosMulta";
            this.aceJurosMulta.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceJurosMulta.Text = "Taxa Juros e Multa";
            // 
            // aceResisoes
            // 
            this.aceResisoes.Name = "aceResisoes";
            this.aceResisoes.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceResisoes.Text = "Rescisões";
            // 
            // aceAtendentes
            // 
            this.aceAtendentes.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceAtendenteCardTI,
            this.aceAtendenteCardTA,
            this.aceAtendentePerfil,
            this.aceAtendenteProjecao,
            this.aceProducaoAgenciador,
            this.accordionControlElement5});
            this.aceAtendentes.HeaderTemplate.AddRange(new DevExpress.XtraBars.Navigation.HeaderElementInfo[] {
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Text),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Image),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.HeaderControl),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.ContextButtons)});
            this.aceAtendentes.Name = "aceAtendentes";
            this.aceAtendentes.Text = "Atendentes";
            // 
            // aceAtendenteCardTI
            // 
            this.aceAtendenteCardTI.Name = "aceAtendenteCardTI";
            this.aceAtendenteCardTI.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceAtendenteCardTI.Text = "Atendente TI Real";
            // 
            // aceAtendenteCardTA
            // 
            this.aceAtendenteCardTA.Name = "aceAtendenteCardTA";
            this.aceAtendenteCardTA.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceAtendenteCardTA.Text = "Atendente TA Real";
            // 
            // aceAtendentePerfil
            // 
            this.aceAtendentePerfil.Name = "aceAtendentePerfil";
            this.aceAtendentePerfil.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceAtendentePerfil.Text = "Atendente Perfil";
            // 
            // aceAtendenteProjecao
            // 
            this.aceAtendenteProjecao.Name = "aceAtendenteProjecao";
            this.aceAtendenteProjecao.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceAtendenteProjecao.Text = "Atendente Projeção";
            // 
            // aceProducaoAgenciador
            // 
            this.aceProducaoAgenciador.Name = "aceProducaoAgenciador";
            this.aceProducaoAgenciador.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceProducaoAgenciador.Text = "Agenciador Produção";
            // 
            // aceComercialP
            // 
            this.aceComercialP.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceAtendenteGrafico});
            this.aceComercialP.Name = "aceComercialP";
            this.aceComercialP.Text = "Comercial Previsão";
            // 
            // aceAtendenteGrafico
            // 
            this.aceAtendenteGrafico.Name = "aceAtendenteGrafico";
            this.aceAtendenteGrafico.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceAtendenteGrafico.Text = "Atendente Produção";
            // 
            // aceContratos
            // 
            this.aceContratos.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceBairroGrafico,
            this.aceContratosAcumulados});
            this.aceContratos.Name = "aceContratos";
            this.aceContratos.Text = "Contratos";
            // 
            // aceBairroGrafico
            // 
            this.aceBairroGrafico.Name = "aceBairroGrafico";
            this.aceBairroGrafico.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceBairroGrafico.Text = "Contratos Ativos (Bairros)";
            // 
            // aceContratosAcumulados
            // 
            this.aceContratosAcumulados.Name = "aceContratosAcumulados";
            this.aceContratosAcumulados.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceContratosAcumulados.Text = "Contratos Acumulados";
            // 
            // aceMetas
            // 
            this.aceMetas.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement1,
            this.accordionControlElement2,
            this.aceMeta1000Contratos});
            this.aceMetas.Name = "aceMetas";
            this.aceMetas.Text = "Metas";
            // 
            // aceMetaTI2018_01
            // 
            this.aceMetaTI2018_01.Name = "aceMetaTI2018_01";
            this.aceMetaTI2018_01.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceMetaTI2018_01.Text = "Meta TI 2019 1º Semestre";
            // 
            // aceMetaTI2018_02
            // 
            this.aceMetaTI2018_02.Name = "aceMetaTI2018_02";
            this.aceMetaTI2018_02.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceMetaTI2018_02.Text = "Meta TI 2018 2º Semestre";
            this.aceMetaTI2018_02.Visible = false;
            // 
            // aceMeta1000Contratos
            // 
            this.aceMeta1000Contratos.Name = "aceMeta1000Contratos";
            this.aceMeta1000Contratos.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceMeta1000Contratos.Text = "Meta 1000 Contratos";
            // 
            // aceFinanceiro
            // 
            this.aceFinanceiro.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceSaldoDia,
            this.aceDRE,
            this.aceContratosPendentes,
            this.aceContratoPag,
            this.aceCapitalizacao,
            this.aceProjecao});
            this.aceFinanceiro.Name = "aceFinanceiro";
            this.aceFinanceiro.Text = "Financeiro";
            // 
            // aceSaldoDia
            // 
            this.aceSaldoDia.Name = "aceSaldoDia";
            this.aceSaldoDia.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceSaldoDia.Text = "Saldo Diário";
            // 
            // aceDRE
            // 
            this.aceDRE.Name = "aceDRE";
            this.aceDRE.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceDRE.Text = "DRE";
            // 
            // aceContratosPendentes
            // 
            this.aceContratosPendentes.Name = "aceContratosPendentes";
            this.aceContratosPendentes.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceContratosPendentes.Text = "Contratos Pendentes";
            // 
            // aceContratoPag
            // 
            this.aceContratoPag.Name = "aceContratoPag";
            this.aceContratoPag.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceContratoPag.Text = "Contratos Pagamento";
            // 
            // aceCapitalizacao
            // 
            this.aceCapitalizacao.Name = "aceCapitalizacao";
            this.aceCapitalizacao.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceCapitalizacao.Text = "Capitalização Imobiliária";
            // 
            // aceProjecao
            // 
            this.aceProjecao.Name = "aceProjecao";
            this.aceProjecao.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceProjecao.Text = "Projeção Mensal";
            // 
            // aceVistoriaFinal
            // 
            this.aceVistoriaFinal.Name = "aceVistoriaFinal";
            this.aceVistoriaFinal.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceVistoriaFinal.Text = "Manutenção e Vistoria Final";
            // 
            // documentManager
            // 
            this.documentManager.ContainerControl = this;
            this.documentManager.RibbonAndBarsMergeStyle = DevExpress.XtraBars.Docking2010.Views.RibbonAndBarsMergeStyle.Always;
            this.documentManager.View = this.tabbedView;
            this.documentManager.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView});
            // 
            // tabbedView
            // 
            this.tabbedView.DocumentGroupProperties.ShowTabHeader = false;
            this.tabbedView.DocumentProperties.AllowClose = false;
            this.tabbedView.RootContainer.Element = null;
            // 
            // dsvContratosResisoess
            // 
            this.dsvContratosResisoess.AllowPrintDashboardItems = true;
            this.dsvContratosResisoess.Location = new System.Drawing.Point(228, 151);
            this.dsvContratosResisoess.Name = "dsvContratosResisoess";
            this.dsvContratosResisoess.Size = new System.Drawing.Size(46, 49);
            this.dsvContratosResisoess.TabIndex = 3;
            // 
            // dsvTaxaAdministracao
            // 
            this.dsvTaxaAdministracao.AllowPrintDashboardItems = true;
            this.dsvTaxaAdministracao.Location = new System.Drawing.Point(299, 151);
            this.dsvTaxaAdministracao.Name = "dsvTaxaAdministracao";
            this.dsvTaxaAdministracao.Size = new System.Drawing.Size(46, 49);
            this.dsvTaxaAdministracao.TabIndex = 6;
            // 
            // dsvTaxaIntermediacao
            // 
            this.dsvTaxaIntermediacao.AllowPrintDashboardItems = true;
            this.dsvTaxaIntermediacao.Location = new System.Drawing.Point(370, 151);
            this.dsvTaxaIntermediacao.Name = "dsvTaxaIntermediacao";
            this.dsvTaxaIntermediacao.Size = new System.Drawing.Size(46, 49);
            this.dsvTaxaIntermediacao.TabIndex = 7;
            // 
            // dsvAtendenteCardTA
            // 
            this.dsvAtendenteCardTA.AllowPrintDashboardItems = true;
            this.dsvAtendenteCardTA.Location = new System.Drawing.Point(725, 206);
            this.dsvAtendenteCardTA.Name = "dsvAtendenteCardTA";
            this.dsvAtendenteCardTA.Size = new System.Drawing.Size(46, 49);
            this.dsvAtendenteCardTA.TabIndex = 8;
            // 
            // dsvAtendenteGrafico
            // 
            this.dsvAtendenteGrafico.AllowPrintDashboardItems = true;
            this.dsvAtendenteGrafico.Location = new System.Drawing.Point(370, 206);
            this.dsvAtendenteGrafico.Name = "dsvAtendenteGrafico";
            this.dsvAtendenteGrafico.Size = new System.Drawing.Size(46, 49);
            this.dsvAtendenteGrafico.TabIndex = 9;
            // 
            // dsvBairroGrafico
            // 
            this.dsvBairroGrafico.AllowPrintDashboardItems = true;
            this.dsvBairroGrafico.Location = new System.Drawing.Point(441, 206);
            this.dsvBairroGrafico.Name = "dsvBairroGrafico";
            this.dsvBairroGrafico.Size = new System.Drawing.Size(46, 49);
            this.dsvBairroGrafico.TabIndex = 10;
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbonControl1;
            // 
            // dsvContratosAtivos
            // 
            this.dsvContratosAtivos.AllowPrintDashboardItems = true;
            this.dsvContratosAtivos.Location = new System.Drawing.Point(228, 206);
            this.dsvContratosAtivos.Name = "dsvContratosAtivos";
            this.dsvContratosAtivos.Size = new System.Drawing.Size(46, 49);
            this.dsvContratosAtivos.TabIndex = 13;
            // 
            // dsvFinanceiroLucroLiquido
            // 
            this.dsvFinanceiroLucroLiquido.AllowPrintDashboardItems = true;
            this.dsvFinanceiroLucroLiquido.Location = new System.Drawing.Point(299, 206);
            this.dsvFinanceiroLucroLiquido.Name = "dsvFinanceiroLucroLiquido";
            this.dsvFinanceiroLucroLiquido.Size = new System.Drawing.Size(46, 49);
            this.dsvFinanceiroLucroLiquido.TabIndex = 16;
            // 
            // dsvFinanPagRecD
            // 
            this.dsvFinanPagRecD.AllowPrintDashboardItems = true;
            this.dsvFinanPagRecD.Location = new System.Drawing.Point(512, 151);
            this.dsvFinanPagRecD.Name = "dsvFinanPagRecD";
            this.dsvFinanPagRecD.Size = new System.Drawing.Size(46, 49);
            this.dsvFinanPagRecD.TabIndex = 19;
            // 
            // dsvFinanceiroPagRecP
            // 
            this.dsvFinanceiroPagRecP.AllowPrintDashboardItems = true;
            this.dsvFinanceiroPagRecP.Location = new System.Drawing.Point(441, 151);
            this.dsvFinanceiroPagRecP.Name = "dsvFinanceiroPagRecP";
            this.dsvFinanceiroPagRecP.Size = new System.Drawing.Size(46, 49);
            this.dsvFinanceiroPagRecP.TabIndex = 20;
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.Location = new System.Drawing.Point(822, 74);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(51, 43);
            this.btnAtualizar.TabIndex = 23;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // dsvContratosPendentes
            // 
            this.dsvContratosPendentes.AllowPrintDashboardItems = true;
            this.dsvContratosPendentes.Location = new System.Drawing.Point(228, 261);
            this.dsvContratosPendentes.Name = "dsvContratosPendentes";
            this.dsvContratosPendentes.Size = new System.Drawing.Size(46, 49);
            this.dsvContratosPendentes.TabIndex = 27;
            // 
            // dsvCustoAtendente
            // 
            this.dsvCustoAtendente.AllowPrintDashboardItems = true;
            this.dsvCustoAtendente.Location = new System.Drawing.Point(299, 261);
            this.dsvCustoAtendente.Name = "dsvCustoAtendente";
            this.dsvCustoAtendente.Size = new System.Drawing.Size(46, 49);
            this.dsvCustoAtendente.TabIndex = 30;
            // 
            // dsvVistoria
            // 
            this.dsvVistoria.AllowPrintDashboardItems = true;
            this.dsvVistoria.Location = new System.Drawing.Point(370, 261);
            this.dsvVistoria.Name = "dsvVistoria";
            this.dsvVistoria.Size = new System.Drawing.Size(46, 49);
            this.dsvVistoria.TabIndex = 33;
            this.dsvVistoria.DashboardItemControlUpdated += new DevExpress.DashboardWin.DashboardItemControlUpdatedEventHandler(this.dsvVistoria_DashboardItemControlUpdated);
            this.dsvVistoria.DashboardItemControlCreated += new DevExpress.DashboardWin.DashboardItemControlCreatedEventHandler(this.dsvVistoria_DashboardItemControlCreated);
            // 
            // dsvCapitalizacao
            // 
            this.dsvCapitalizacao.AllowPrintDashboardItems = true;
            this.dsvCapitalizacao.Location = new System.Drawing.Point(512, 206);
            this.dsvCapitalizacao.Name = "dsvCapitalizacao";
            this.dsvCapitalizacao.Size = new System.Drawing.Size(46, 49);
            this.dsvCapitalizacao.TabIndex = 36;
            // 
            // dsvContratosAcumulados
            // 
            this.dsvContratosAcumulados.AllowPrintDashboardItems = true;
            this.dsvContratosAcumulados.Location = new System.Drawing.Point(654, 270);
            this.dsvContratosAcumulados.Name = "dsvContratosAcumulados";
            this.dsvContratosAcumulados.Size = new System.Drawing.Size(46, 49);
            this.dsvContratosAcumulados.TabIndex = 39;
            // 
            // dsvFinanPagRecDetalheFilial
            // 
            this.dsvFinanPagRecDetalheFilial.AllowPrintDashboardItems = true;
            this.dsvFinanPagRecDetalheFilial.Location = new System.Drawing.Point(725, 270);
            this.dsvFinanPagRecDetalheFilial.Name = "dsvFinanPagRecDetalheFilial";
            this.dsvFinanPagRecDetalheFilial.Size = new System.Drawing.Size(46, 49);
            this.dsvFinanPagRecDetalheFilial.TabIndex = 42;
            // 
            // dsvCheques
            // 
            this.dsvCheques.AllowPrintDashboardItems = true;
            this.dsvCheques.Location = new System.Drawing.Point(228, 365);
            this.dsvCheques.Name = "dsvCheques";
            this.dsvCheques.Size = new System.Drawing.Size(46, 49);
            this.dsvCheques.TabIndex = 48;
            // 
            // dsvAtendenteCardTI
            // 
            this.dsvAtendenteCardTI.AllowPrintDashboardItems = true;
            this.dsvAtendenteCardTI.Location = new System.Drawing.Point(299, 365);
            this.dsvAtendenteCardTI.Name = "dsvAtendenteCardTI";
            this.dsvAtendenteCardTI.Size = new System.Drawing.Size(46, 49);
            this.dsvAtendenteCardTI.TabIndex = 51;
            // 
            // dsvJurosMulta
            // 
            this.dsvJurosMulta.AllowPrintDashboardItems = true;
            this.dsvJurosMulta.Location = new System.Drawing.Point(441, 261);
            this.dsvJurosMulta.Name = "dsvJurosMulta";
            this.dsvJurosMulta.Size = new System.Drawing.Size(46, 49);
            this.dsvJurosMulta.TabIndex = 54;
            // 
            // btnInfo
            // 
            this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnInfo.Image")));
            this.btnInfo.Location = new System.Drawing.Point(765, 74);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(50, 43);
            this.btnInfo.TabIndex = 57;
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // dsvMetaTI2018_1
            // 
            this.dsvMetaTI2018_1.AllowPrintDashboardItems = true;
            this.dsvMetaTI2018_1.Location = new System.Drawing.Point(370, 365);
            this.dsvMetaTI2018_1.Name = "dsvMetaTI2018_1";
            this.dsvMetaTI2018_1.Size = new System.Drawing.Size(46, 49);
            this.dsvMetaTI2018_1.TabIndex = 60;
            // 
            // dsvMetaTI2018_2
            // 
            this.dsvMetaTI2018_2.AllowPrintDashboardItems = true;
            this.dsvMetaTI2018_2.Location = new System.Drawing.Point(583, 151);
            this.dsvMetaTI2018_2.Name = "dsvMetaTI2018_2";
            this.dsvMetaTI2018_2.Size = new System.Drawing.Size(46, 49);
            this.dsvMetaTI2018_2.TabIndex = 63;
            // 
            // dsvContratopag
            // 
            this.dsvContratopag.AllowPrintDashboardItems = true;
            this.dsvContratopag.Location = new System.Drawing.Point(583, 206);
            this.dsvContratopag.Name = "dsvContratopag";
            this.dsvContratopag.Size = new System.Drawing.Size(46, 49);
            this.dsvContratopag.TabIndex = 64;
            // 
            // dsvSaldoDia
            // 
            this.dsvSaldoDia.AllowPrintDashboardItems = true;
            this.dsvSaldoDia.Location = new System.Drawing.Point(512, 261);
            this.dsvSaldoDia.Name = "dsvSaldoDia";
            this.dsvSaldoDia.Size = new System.Drawing.Size(46, 49);
            this.dsvSaldoDia.TabIndex = 67;
            // 
            // dsv_dre
            // 
            this.dsv_dre.AllowPrintDashboardItems = true;
            this.dsv_dre.Location = new System.Drawing.Point(441, 365);
            this.dsv_dre.Name = "dsv_dre";
            this.dsv_dre.Size = new System.Drawing.Size(46, 49);
            this.dsv_dre.TabIndex = 70;
            // 
            // dsv_projecao
            // 
            this.dsv_projecao.AllowPrintDashboardItems = true;
            this.dsv_projecao.Location = new System.Drawing.Point(512, 365);
            this.dsv_projecao.Name = "dsv_projecao";
            this.dsv_projecao.Size = new System.Drawing.Size(46, 49);
            this.dsv_projecao.TabIndex = 73;
            // 
            // accordionControlElement6
            // 
            this.accordionControlElement6.Name = "accordionControlElement6";
            this.accordionControlElement6.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement6.Text = "Manutenção e Vistoria Final";
            // 
            // dsv_atendente_proj
            // 
            this.dsv_atendente_proj.AllowPrintDashboardItems = true;
            this.dsv_atendente_proj.Location = new System.Drawing.Point(583, 261);
            this.dsv_atendente_proj.Name = "dsv_atendente_proj";
            this.dsv_atendente_proj.Size = new System.Drawing.Size(46, 49);
            this.dsv_atendente_proj.TabIndex = 76;
            // 
            // dsv_atendente_perfil
            // 
            this.dsv_atendente_perfil.AllowPrintDashboardItems = true;
            this.dsv_atendente_perfil.Location = new System.Drawing.Point(654, 206);
            this.dsv_atendente_perfil.Name = "dsv_atendente_perfil";
            this.dsv_atendente_perfil.Size = new System.Drawing.Size(46, 49);
            this.dsv_atendente_perfil.TabIndex = 77;
            // 
            // dsvMeta1000
            // 
            this.dsvMeta1000.AllowPrintDashboardItems = true;
            this.dsvMeta1000.Location = new System.Drawing.Point(654, 151);
            this.dsvMeta1000.Name = "dsvMeta1000";
            this.dsvMeta1000.Size = new System.Drawing.Size(46, 49);
            this.dsvMeta1000.TabIndex = 80;
            // 
            // dsvProducaoAgenciador
            // 
            this.dsvProducaoAgenciador.AllowPrintDashboardItems = true;
            this.dsvProducaoAgenciador.Location = new System.Drawing.Point(725, 151);
            this.dsvProducaoAgenciador.Name = "dsvProducaoAgenciador";
            this.dsvProducaoAgenciador.Size = new System.Drawing.Size(46, 49);
            this.dsvProducaoAgenciador.TabIndex = 83;
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceMetaTI2018_02,
            this.aceMetaTI2018_01});
            this.accordionControlElement1.Name = "accordionControlElement1";
            this.accordionControlElement1.Text = "Metas 2018/2019";
            // 
            // accordionControlElement2
            // 
            this.accordionControlElement2.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement3,
            this.accordionControlElement4});
            this.accordionControlElement2.Name = "accordionControlElement2";
            this.accordionControlElement2.Text = "Metas 2020";
            // 
            // accordionControlElement3
            // 
            this.accordionControlElement3.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceRobsonVisaoGeral,
            this.aceMetaQuantidadeContratos,
            this.acePremiacaoAtendente});
            this.accordionControlElement3.Name = "accordionControlElement3";
            this.accordionControlElement3.Text = "Robson";
            // 
            // accordionControlElement4
            // 
            this.accordionControlElement4.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceMetaLideres50,
            this.aceMetaLideres100});
            this.accordionControlElement4.Name = "accordionControlElement4";
            this.accordionControlElement4.Text = "Lideres";
            // 
            // aceRobsonVisaoGeral
            // 
            this.aceRobsonVisaoGeral.Name = "aceRobsonVisaoGeral";
            this.aceRobsonVisaoGeral.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceRobsonVisaoGeral.Text = "Metas Robson Visão Geral";
            // 
            // aceMetaQuantidadeContratos
            // 
            this.aceMetaQuantidadeContratos.Name = "aceMetaQuantidadeContratos";
            this.aceMetaQuantidadeContratos.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceMetaQuantidadeContratos.Text = "Meta Quantidade Contratos";
            // 
            // acePremiacaoAtendente
            // 
            this.acePremiacaoAtendente.Name = "acePremiacaoAtendente";
            this.acePremiacaoAtendente.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.acePremiacaoAtendente.Text = "Premiação por Atendente";
            // 
            // dsvRobsonVisaoGeral
            // 
            this.dsvRobsonVisaoGeral.AllowPrintDashboardItems = true;
            this.dsvRobsonVisaoGeral.Location = new System.Drawing.Point(583, 365);
            this.dsvRobsonVisaoGeral.Name = "dsvRobsonVisaoGeral";
            this.dsvRobsonVisaoGeral.Size = new System.Drawing.Size(46, 49);
            this.dsvRobsonVisaoGeral.TabIndex = 86;
            // 
            // dsvMetaQuantidadeContratos
            // 
            this.dsvMetaQuantidadeContratos.AllowPrintDashboardItems = true;
            this.dsvMetaQuantidadeContratos.Location = new System.Drawing.Point(654, 365);
            this.dsvMetaQuantidadeContratos.Name = "dsvMetaQuantidadeContratos";
            this.dsvMetaQuantidadeContratos.Size = new System.Drawing.Size(46, 49);
            this.dsvMetaQuantidadeContratos.TabIndex = 87;
            // 
            // dsvPremiacaoAtendente
            // 
            this.dsvPremiacaoAtendente.AllowPrintDashboardItems = true;
            this.dsvPremiacaoAtendente.Location = new System.Drawing.Point(725, 365);
            this.dsvPremiacaoAtendente.Name = "dsvPremiacaoAtendente";
            this.dsvPremiacaoAtendente.Size = new System.Drawing.Size(46, 49);
            this.dsvPremiacaoAtendente.TabIndex = 88;
            // 
            // aceMetaLideres50
            // 
            this.aceMetaLideres50.Name = "aceMetaLideres50";
            this.aceMetaLideres50.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceMetaLideres50.Text = "Meta Lideres 50% Salário";
            // 
            // dsvMetaLideres50
            // 
            this.dsvMetaLideres50.AllowPrintDashboardItems = true;
            this.dsvMetaLideres50.Location = new System.Drawing.Point(228, 420);
            this.dsvMetaLideres50.Name = "dsvMetaLideres50";
            this.dsvMetaLideres50.Size = new System.Drawing.Size(46, 49);
            this.dsvMetaLideres50.TabIndex = 89;
            // 
            // aceMetaLideres100
            // 
            this.aceMetaLideres100.Name = "aceMetaLideres100";
            this.aceMetaLideres100.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceMetaLideres100.Text = "Meta Lideres 100% Salário";
            // 
            // dsvMetaLideres100
            // 
            this.dsvMetaLideres100.AllowPrintDashboardItems = true;
            this.dsvMetaLideres100.Location = new System.Drawing.Point(299, 420);
            this.dsvMetaLideres100.Name = "dsvMetaLideres100";
            this.dsvMetaLideres100.Size = new System.Drawing.Size(46, 49);
            this.dsvMetaLideres100.TabIndex = 90;
            // 
            // accordionControlElement5
            // 
            this.accordionControlElement5.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.aceVisaoGeralComissoes,
            this.aceDetalhamentoComissoes});
            this.accordionControlElement5.Name = "accordionControlElement5";
            this.accordionControlElement5.Text = "Comissões";
            // 
            // aceVisaoGeralComissoes
            // 
            this.aceVisaoGeralComissoes.Name = "aceVisaoGeralComissoes";
            this.aceVisaoGeralComissoes.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceVisaoGeralComissoes.Text = "Visão Geral Comissões";
            // 
            // aceDetalhamentoComissoes
            // 
            this.aceDetalhamentoComissoes.Name = "aceDetalhamentoComissoes";
            this.aceDetalhamentoComissoes.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.aceDetalhamentoComissoes.Text = "Detalhamento Comissões";
            // 
            // dsvVisaoGeralComissoes
            // 
            this.dsvVisaoGeralComissoes.AllowPrintDashboardItems = true;
            this.dsvVisaoGeralComissoes.Location = new System.Drawing.Point(370, 420);
            this.dsvVisaoGeralComissoes.Name = "dsvVisaoGeralComissoes";
            this.dsvVisaoGeralComissoes.Size = new System.Drawing.Size(46, 49);
            this.dsvVisaoGeralComissoes.TabIndex = 91;
            // 
            // dsvDetalhamentoComissoes
            // 
            this.dsvDetalhamentoComissoes.AllowPrintDashboardItems = true;
            this.dsvDetalhamentoComissoes.Location = new System.Drawing.Point(441, 420);
            this.dsvDetalhamentoComissoes.Name = "dsvDetalhamentoComissoes";
            this.dsvDetalhamentoComissoes.Size = new System.Drawing.Size(46, 49);
            this.dsvDetalhamentoComissoes.TabIndex = 92;
            // 
            // frmConsultaAdm
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 494);
            this.Controls.Add(this.dsvDetalhamentoComissoes);
            this.Controls.Add(this.dsvVisaoGeralComissoes);
            this.Controls.Add(this.dsvMetaLideres100);
            this.Controls.Add(this.dsvMetaLideres50);
            this.Controls.Add(this.dsvPremiacaoAtendente);
            this.Controls.Add(this.dsvMetaQuantidadeContratos);
            this.Controls.Add(this.dsvRobsonVisaoGeral);
            this.Controls.Add(this.dsvProducaoAgenciador);
            this.Controls.Add(this.dsvMeta1000);
            this.Controls.Add(this.dsv_atendente_perfil);
            this.Controls.Add(this.dsv_atendente_proj);
            this.Controls.Add(this.dsv_projecao);
            this.Controls.Add(this.dsv_dre);
            this.Controls.Add(this.dsvSaldoDia);
            this.Controls.Add(this.dsvContratopag);
            this.Controls.Add(this.dsvMetaTI2018_2);
            this.Controls.Add(this.dsvMetaTI2018_1);
            this.Controls.Add(this.dsvJurosMulta);
            this.Controls.Add(this.dsvAtendenteCardTI);
            this.Controls.Add(this.dsvCheques);
            this.Controls.Add(this.dsvFinanPagRecDetalheFilial);
            this.Controls.Add(this.dsvCapitalizacao);
            this.Controls.Add(this.dsvVistoria);
            this.Controls.Add(this.dsvCustoAtendente);
            this.Controls.Add(this.dsvContratosPendentes);
            this.Controls.Add(this.dsvFinanPagRecD);
            this.Controls.Add(this.dsvFinanceiroLucroLiquido);
            this.Controls.Add(this.dsvAtendenteGrafico);
            this.Controls.Add(this.dsvFinanceiroPagRecP);
            this.Controls.Add(this.dsvBairroGrafico);
            this.Controls.Add(this.dsvTaxaIntermediacao);
            this.Controls.Add(this.dsvTaxaAdministracao);
            this.Controls.Add(this.dsvContratosAcumulados);
            this.Controls.Add(this.dsvAtendenteCardTA);
            this.Controls.Add(this.dsvContratosAtivos);
            this.Controls.Add(this.dsvContratosResisoess);
            this.Controls.Add(this.btnInfo);
            this.Controls.Add(this.btnAtualizar);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.ribbonControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConsultaAdm";
            this.Ribbon = this.ribbonControl1;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultas Gerencias";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanel.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosResisoess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvTaxaAdministracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvTaxaIntermediacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvAtendenteCardTA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvAtendenteGrafico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvBairroGrafico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosAtivos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanceiroLucroLiquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanPagRecD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanceiroPagRecP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosPendentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvCustoAtendente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvVistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvCapitalizacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratosAcumulados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvFinanPagRecDetalheFilial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvCheques)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvAtendenteCardTI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvJurosMulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaTI2018_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaTI2018_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvContratopag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvSaldoDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_dre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_projecao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_atendente_proj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsv_atendente_perfil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMeta1000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvProducaoAgenciador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvRobsonVisaoGeral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaQuantidadeContratos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvPremiacaoAtendente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaLideres50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvMetaLideres100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvVisaoGeralComissoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsvDetalhamentoComissoes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceGroupHistorico;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceTaxaIntermediacao;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceAtendenteCardTA;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceAtendenteGrafico;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView;
        private DevExpress.DashboardWin.DashboardViewer dsvContratosResisoess;
        private DevExpress.DashboardWin.DashboardViewer dsvBairroGrafico;
        private DevExpress.DashboardWin.DashboardViewer dsvAtendenteGrafico;
        private DevExpress.DashboardWin.DashboardViewer dsvAtendenteCardTA;
        private DevExpress.DashboardWin.DashboardViewer dsvTaxaIntermediacao;
        private DevExpress.DashboardWin.DashboardViewer dsvTaxaAdministracao;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.DashboardWin.DashboardViewer dsvContratosAtivos;
        private DevExpress.DashboardWin.DashboardViewer dsvFinanceiroLucroLiquido;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceContratos;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceBairroGrafico;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceResisoes;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceFinanceiro;
        private DevExpress.DashboardWin.DashboardViewer dsvFinanPagRecD;
        private DevExpress.DashboardWin.DashboardViewer dsvFinanceiroPagRecP;
        private System.Windows.Forms.Button btnAtualizar;
        private DevExpress.DashboardWin.DashboardViewer dsvContratosPendentes;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceContratosPendentes;
        private DevExpress.DashboardWin.DashboardViewer dsvCustoAtendente;
        private DevExpress.DashboardWin.DashboardViewer dsvVistoria;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceVistoriaFinal;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceCapitalizacao;
        private DevExpress.DashboardWin.DashboardViewer dsvCapitalizacao;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceComercialP;
        public DevExpress.XtraBars.Docking.DockPanel dockPanel;
        public DevExpress.XtraBars.Navigation.AccordionControl accordionControl;
        public DevExpress.XtraBars.Navigation.AccordionControlElement aceTaxaAdministracao;
        private DevExpress.DashboardWin.DashboardViewer dsvContratosAcumulados;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceContratosAcumulados;
        private DevExpress.DashboardWin.DashboardViewer dsvFinanPagRecDetalheFilial;
        private DevExpress.DashboardWin.DashboardViewer dsvCheques;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceAtendenteCardTI;
        private DevExpress.DashboardWin.DashboardViewer dsvAtendenteCardTI;
        private DevExpress.DashboardWin.DashboardViewer dsvJurosMulta;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceJurosMulta;
        private System.Windows.Forms.Button btnInfo;
        private DevExpress.DashboardWin.DashboardViewer dsvMetaTI2018_1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceMetas;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceMetaTI2018_01;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceMetaTI2018_02;
        private DevExpress.DashboardWin.DashboardViewer dsvMetaTI2018_2;
        private DevExpress.DashboardWin.DashboardViewer dsvContratopag;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceContratoPag;
        private DevExpress.DashboardWin.DashboardViewer dsvSaldoDia;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceSaldoDia;
        private DevExpress.DashboardWin.DashboardViewer dsv_dre;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceDRE;
        private DevExpress.DashboardWin.DashboardViewer dsv_projecao;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceProjecao;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceAtendentes;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceAtendentePerfil;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceAtendenteProjecao;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement6;
        private DevExpress.DashboardWin.DashboardViewer dsv_atendente_proj;
        private DevExpress.DashboardWin.DashboardViewer dsv_atendente_perfil;
        private DevExpress.DashboardWin.DashboardViewer dsvMeta1000;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceMeta1000Contratos;
        private DevExpress.DashboardWin.DashboardViewer dsvProducaoAgenciador;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceProducaoAgenciador;
        private DevExpress.DashboardWin.DashboardViewer dsvRobsonVisaoGeral;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement2;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceRobsonVisaoGeral;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceMetaQuantidadeContratos;
        private DevExpress.XtraBars.Navigation.AccordionControlElement acePremiacaoAtendente;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement4;
        private DevExpress.DashboardWin.DashboardViewer dsvMetaQuantidadeContratos;
        private DevExpress.DashboardWin.DashboardViewer dsvPremiacaoAtendente;
        private DevExpress.DashboardWin.DashboardViewer dsvMetaLideres50;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceMetaLideres50;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceMetaLideres100;
        private DevExpress.DashboardWin.DashboardViewer dsvMetaLideres100;
        private DevExpress.DashboardWin.DashboardViewer dsvDetalhamentoComissoes;
        private DevExpress.DashboardWin.DashboardViewer dsvVisaoGeralComissoes;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement5;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceVisaoGeralComissoes;
        private DevExpress.XtraBars.Navigation.AccordionControlElement aceDetalhamentoComissoes;
    }
}

