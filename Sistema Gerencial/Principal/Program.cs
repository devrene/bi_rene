﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using System.Globalization;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Deployment.Application;
using System.Reflection;

namespace SisGerencial
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("pt-BR");

            
            frmLogin frmlogin = new frmLogin();
            frmlogin.ShowDialog();

            if (frmlogin.logado && !(frmlogin.atualizar))
            {
                frmConsultaAdm principal = new frmConsultaAdm();
                BonusSkins.Register();
                SkinManager.EnableFormSkins();
                Application.Run(principal);
            }
        }
    }
}

